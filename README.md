# hostapd

IEEE 802.11 AP and IEEE 802.1X/WPA/WPA2/EAP Authenticator. http://w1.fi/hostapd

* source wpa

## Unofficial documentation
### Wifi interface identification
* ip link show
* [linux how to list wireless interfaces](https://google.com/search?q=linux+how+to+list+wireless+interfaces)
* [*Linux Show / Display Available Network Interfaces*
  ](https://www.cyberciti.biz/faq/linux-list-network-interfaces-names-command/)

### Access-point setup
* [linux wifi access point](https://google.com/search?q=linux+wifi+access+point)
* [*Debian / Ubuntu Linux: Setup Wireless Access Point (WAP) with Hostapd*
  ](https://www.cyberciti.biz/faq/debian-ubuntu-linux-setting-wireless-access-point/)
  2016
* [*Turn any computer into a wireless access point with Hostapd*
  ](https://seravo.fi/2014/create-wireless-access-point-hostapd)
  2014
* [*Software access point*](https://wiki.archlinux.org/index.php/software_access_point)
  ArchLinux
* [*Setting WiFi Access Point Mode in Linux*
  ](https://developer.toradex.com/knowledge-base/wireless-access-point-mode)
  2019
* [*How To Setup A Wireless Access Point On Linux OS using hostapd*
  ](https://www.shellvoide.com/wifi/setup-wireless-access-point-hostapd-dnsmasq-linux/)
  2018 hash3liZer
* [*Chapter 4. Building a Linux Wireless Access Point*
  ](https://www.oreilly.com/library/view/linux-networking-cookbook/9780596102487/ch04.html)
* [*How to setup an Access Point mode Wi-Fi Hotspot?*
  ](https://askubuntu.com/questions/180733/how-to-setup-an-access-point-mode-wi-fi-hotspot)
  (2019)
* (fr) [*HostAP daemon*](https://doc.ubuntu-fr.org/hostapd)

### Kernel libraries
* [*How nl80211 library & cfg80211 work?*
  ](https://stackoverflow.com/questions/21456235/how-nl80211-library-cfg80211-work)
  (2019)